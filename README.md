## Installation

```bash
$ npm install -g ts-node
$ npm install
```

## Build project

```bash
$ npm install -g tsc
$ npm run build
```

## Running the app

```bash
# development
$ npm run dev

# production mode
$ npm run start
```

## Default CMS admin 

```bash
mysql$ INSERT INTO `user`(`username`, `password`) VALUES ('admin', 'admin')
```