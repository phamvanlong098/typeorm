require('dotenv').config()
import * as path from 'path'
import * as express from 'express'
import * as cookieParser from 'cookie-parser'
import * as methodOverride from 'method-override'
import * as session from 'express-session'
import * as cors from 'cors'
import * as morgan from 'morgan'
import * as bodyParser from 'body-parser'
import * as jwt from 'jsonwebtoken'
import { Request, Response } from 'express'
const port = parseInt(process.env.PORT)
const secret: string = process.env.INTERNAL_SECRET
import routes from './routes/routes'

const whitelist = process.env.WHITE_LIST

const corsOptionsDelegate = {
  origin: 'http://localhost:3000',
  credentials: true,
  exposedHeaders: ['set-cookie'],
}

// var corsOptionsDelegate = function (req: Request, callback) {
//   var corsOptions
//   if (whitelist.indexOf(req.header('Origin')) !== -1) {
//     corsOptions = {
//       origin: true,
//       credentials: true,
//       exposedHeaders: ['set-cookie'],
//     }
//   } else {
//     corsOptions = {
//       origin: false,
//       credentials: true,
//       exposedHeaders: ['set-cookie'],
//     }
//   }
//   callback(null, corsOptions)
// }

const app = express()
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('_method'))
app.use(cookieParser())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(morgan('tiny'))
// app.use(morgan('combined'))
app.use(cors(corsOptionsDelegate))

declare module 'express-session' {
  export interface SessionData {
    token: string
    username: string
    secret: string
  }
}
app.use(
  session({
    secret: secret,
    resave: false,
    saveUninitialized: true,
    cookie: {
      secure: false,
      maxAge: 30 * 60 * 1000,
    },
  })
)

// test middleWare
app.use('/', (req: Request, res: Response, next) => {
  req.session.secret = secret
  next()
})

routes(app)

// start express server
app.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}`)
})
