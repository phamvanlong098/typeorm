import { Router } from 'express'
import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { helper as authHelper } from '../middlewares/auth'

export default {
  login: async (req: Request, res: Response) => {
    const username = req.body.username
    const password = req.body.password
    if (!username || !password) {
      res.json({ msg: 'Username or password incorrect' })
      return
    }
    const userRepository = AppDataSource.getRepository(User)
    const user = await userRepository.findOneBy({ username, password })
    let result = {}
    if (user) {
      req.session.username = user.username
      const filtered = helper.filterPropUser(user)
      result = Object.assign({}, result, filtered, {
        access_token: authHelper.setToken(req, filtered),
      })
    }
    res.json(result)
  },
  logout: (req: Request, res: Response) => {
    req.session.destroy((err) => {
      if (err) console.log('Logout-error: ', err)
    })
    res.json({ msg: 'Lougout success' })
  },
}
