import { Request, Response } from 'express'
import { Article } from '../database/entity/news/Article.entity'
import { Category } from '../database/entity/Category.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { read } from 'fs'

export default {
  // [GET] /article?page=1
  getArticles: async function (req: Request, res: Response) {
    const articleRepository = await AppDataSource.getRepository(Article)
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const result = await helper.paginationByCategory(
      articleRepository,
      pageNumber,
      pageSize,
      '1'
    )
    const resultFilter = { ...result }
    helper.filterArticles(resultFilter.data)
    res.json(resultFilter)
  },

  // [GET] /article/:id
  getArticle: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    try {
      const article = await AppDataSource.getRepository(Article)
        .createQueryBuilder('article')
        .innerJoinAndSelect('article.category', 'category')
        .where('article.id = :id', { id: id })
        .getOne()
      res.json(article)
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [POST] /article
  postArticle: async function (req: Request, res: Response) {
    const articleRepository = AppDataSource.getRepository(Article)
    const categoryRepository = AppDataSource.getRepository(Category)
    const categoryId = Number(req.body.categoryId)

    try {
      const article = new Article()
      article.category = await helper.checkFindOne(
        res,
        categoryRepository,
        { id: categoryId },
        ['categoryId is invalid', 'categoryId must be an integer number']
      )
      article.active = Boolean(req.body.active)
      article.editor = req.session.username
      article.headline = String(req.body.headline)
      article.html = String(req.body.html)
      article.publishedAt = new Date()
      article.thumbnail = String(req.body.thumbnail)
      article.views = 0

      await helper.validate(res, article)
      const result = await articleRepository.save(article)
      res.json({ msg: 'insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /article/:id
  updateArticle: async function (req: Request, res: Response) {
    const articleRepository = AppDataSource.getRepository(Article)
    const categoryRepository = AppDataSource.getRepository(Category)
    const id = Number(req.params.id)
    const categoryId = Number(req.body.categoryId)
    try {
      const article = await helper.checkFindOne(res, articleRepository, {
        id: id,
      })
      article.category = await helper.checkFindOne(
        res,
        categoryRepository,
        { id: categoryId },
        ['categoryId is invalid', 'categoryId must be an integer number']
      )
      article.active = Boolean(req.body.active)
      article.editor = req.session.username
      article.headline = String(req.body.headline)
      article.html = String(req.body.html)
      article.publishedAt = new Date()
      article.thumbnail = String(req.body.thumbnail)

      await helper.validate(res, article)
      const result = await articleRepository.save(article)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /article/:id
  deleteArticle: async function (req: Request, res: Response) {
    const articleRepository = AppDataSource.getRepository(Article)
    const id = Number(req.params.id)
    try {
      const article = await articleRepository.findOneBy({ id: id })
      const result = await articleRepository.remove(article)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] article/columns
  getColumns: function (req: Request, res: Response) {
    const columns = {
      active: 'boolean',
      categoryId: 'number',
      thumbnail: 'string',
      headline: 'string',
      html: 'string',
    }
    res.json(columns)
  },
}
