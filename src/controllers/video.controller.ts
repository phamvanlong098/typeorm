import { Request, Response } from 'express'
import { Video } from '../database/entity/media/Video.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { Category } from '../database/entity/Category.entity'

export default {
  // [GET] /video?page=1
  getVideos: async function (req: Request, res: Response) {
    const videoRepository = await AppDataSource.getRepository(Video)
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const result = await helper.paginationByCategory(
      videoRepository,
      pageNumber,
      pageSize,
      '1'
    )
    res.json(result)
  },

  // [GET] /video/:id
  getVideo: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    try {
      const video = await AppDataSource.getRepository(Video)
        .createQueryBuilder('video')
        .innerJoinAndSelect('video.category', 'category')
        .where('video.id = :id', { id: id })
        .getOne()
      res.json(video)
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [POST] /video
  postVideo: async function (req: Request, res: Response) {
    const videoRepository = AppDataSource.getRepository(Video)
    const categoryRepository = AppDataSource.getRepository(Category)
    const categoryId = Number(req.body.categoryId)

    try {
      const video = new Video()
      video.active = Boolean(req.body.active)
      video.category = await helper.checkFindOne(
        res,
        categoryRepository,
        { id: categoryId },
        ['categoryId is invalid', 'categoryId must be an integer number']
      )
      video.editor = req.session.username
      video.thumbnail = String(req.body.thumbnail)
      video.title = String(req.body.title)
      video.videoUrl = String(req.body.videoUrl)
      video.publishedAt = new Date()

      await helper.validate(res, video)
      const result = await videoRepository.save(video)
      res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [PUT] /video/:id
  updateVideo: async function (req: Request, res: Response) {
    const videoRepository = AppDataSource.getRepository(Video)
    const categoryRepository = AppDataSource.getRepository(Category)
    const categoryId = Number(req.body.categoryId)

    const id = Number(req.params.id)
    try {
      const video = await videoRepository.findOneBy({ id: id })
      video.category = await helper.checkFindOne(
        res,
        categoryRepository,
        { id: categoryId },
        ['categoryId is invalid', 'categoryId must be an integer number']
      )
      video.active = Boolean(req.body.active)
      video.editor = req.session.username
      video.thumbnail = String(req.body.thumbnail)
      video.title = String(req.body.title)
      video.videoUrl = String(req.body.videoUrl)
      video.publishedAt = new Date()

      await helper.validate(res, video)
      const result = await videoRepository.save(video)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {}
  },

  // [DELETE] /video/:id
  deleteVideo: async function (req: Request, res: Response) {
    const videoRepository = AppDataSource.getRepository(Video)
    const id = Number(req.params.id)
    try {
      const video = await videoRepository.findOneBy({ id: id })
      const result = await videoRepository.remove(video)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] video/columns
  getColumns: function (req: Request, res: Response) {
    const columns = {
      active: 'boolean',
      categoryId: 'number',
      thumbnail: 'string',
      title: 'string',
      videoUrl: 'string',
    }
    res.json(columns)
  },
}
