import { Request, Response } from 'express'
import { Category } from '../database/entity/Category.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { Article } from '../database/entity/news/Article.entity'
import { Video } from '../database/entity/media/Video.entity'

export default {
  // [GET] /category?page=1
  getCategories: async function (req: Request, res: Response) {
    const categoryRepository = await AppDataSource.getRepository(Category)
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const result = await helper.pagination(
      categoryRepository,
      pageNumber,
      pageSize
    )
    res.json(result)
  },

  // [GET] /category/:id
  getCategory: async function (req: Request, res: Response) {
    const id = Number(req.params.id)
    const category = await AppDataSource.getRepository(Category).findOneBy({
      id: id,
    })
    res.json(category)
  },

  // [POST] /category
  postCategory: async function (req: Request, res: Response) {
    const categoryRepository = AppDataSource.getRepository(Category)

    const category = new Category()
    category.active = Boolean(req.body.active)
    category.name = String(req.body.name)
    await helper.validate(res, category)
    const result = await categoryRepository.save(category)
    res.json({ msg: 'insert complete', result: JSON.stringify(result) })
  },

  // [PUT] /category/:id
  updateCategory: async function (req: Request, res: Response) {
    const categoryRepository = AppDataSource.getRepository(Category)
    const id = Number(req.params.id)
    try {
      const category = await categoryRepository.findOneBy({ id: id })
      category.active = Boolean(req.body.active)
      category.name = String(req.body.name)

      await helper.validate(res, category)
      const result = await categoryRepository.save(category)
      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [DELETE] /category/:id
  deleteCategory: async function (req: Request, res: Response) {
    const categoryRepository = AppDataSource.getRepository(Category)
    const id = Number(req.params.id)
    try {
      const category = await categoryRepository.findOneBy({ id: id })
      const result = await categoryRepository.remove(category)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] /category/:categoryId/article
  getArticles: async function (req: Request, res: Response) {
    const articleRepository = AppDataSource.getRepository(Article)
    const id = Number(req.params.categoryId)
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    try {
      const result = await helper.paginationByCategory(
        articleRepository,
        pageNumber,
        pageSize,
        `category.id = ${id}`
      )
      const resultFilter = { ...result }
      helper.filterArticles(resultFilter.data)
      res.json(resultFilter)
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] /category/:categoryId/video
  getVideos: async function (req: Request, res: Response) {
    const videoRepository = AppDataSource.getRepository(Video)
    const id = Number(req.params.categoryId)
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    try {
      const result = await helper.paginationByCategory(
        videoRepository,
        pageNumber,
        pageSize,
        `category.id = ${id}`
      )
      res.json(result)
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] article/columns
  getColumns: function (req: Request, res: Response) {
    const columns = {
      active: 'boolean',
      name: 'string',
    }
    res.json(columns)
  },
}
