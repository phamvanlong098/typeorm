import { validate } from 'class-validator'
import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import { Category } from '../database/entity/Category.entity'

const helper = {
  async pagination(tableRepository, pageNumber, pageSize = 10, where = '1') {
    if (pageSize > 20) pageSize = 20
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  async paginationByCategory(
    tableRepository,
    pageNumber,
    pageSize = 10,
    where = '1'
  ) {
    if (pageSize > 20) pageSize = 20
    const [data, count] = await tableRepository
      .createQueryBuilder('alias')
      .leftJoinAndSelect('alias.category', 'category')
      .where(where)
      .skip(Number((pageNumber - 1) * pageSize))
      .take(pageSize)
      .getManyAndCount()
    return Object.assign({}, { total: count, data: data })
  },

  filterPropUser(user) {
    const result = {
      username: user.username,
      email: user.email,
    }
    return result
  },

  filterConstraints(error) {
    const constraints = []
    error.forEach((err) => {
      for (let key in err.constraints) {
        constraints.push(err.constraints[key])
      }
    })
    return constraints
  },

  filterArticle(article) {
    article.html = ''
  },

  filterArticles(articles) {
    articles.forEach((article) => helper.filterArticle(article))
  },

  async checkFindOne(res, repository, where, errorMsg: any = 'Invalid id') {
    try {
      const result = await repository.findOneBy(where)
      if (!result) throw new Error()
      return result
    } catch (error) {
      res.json({
        msg: 'Invalid id',
        error: errorMsg,
      })
      throw new Error('Invalid id')
    }
  },

  async validate(res, item) {
    const errors = await validate(item)
    if (errors.length > 0) {
      res.json({
        msg: `Validation failed!`,
        error: helper.filterConstraints(errors),
      })
      throw new Error(`Validation failed!`)
    }
  },
}

export default helper
