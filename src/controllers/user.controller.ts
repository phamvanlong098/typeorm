import { Request, Response } from 'express'
import { User } from '../database/entity/User.entity'
import AppDataSource from '../database/connection'
import helper from './helper'
import { resolve } from 'path'

export default {
  // [GET] /user/?page=1
  getUsers: async function (req: Request, res: Response) {
    const pageNumber = Number(req.query.page) || 1
    const pageSize = Number(req.query.pageSize) || 10
    const userRepository = AppDataSource.getRepository(User)
    const result = await helper.pagination(userRepository, pageNumber, pageSize)
    res.json(result)
  },

  // [GET] /user/:username
  getUser: async function (req: Request, res: Response) {
    const username = req.params.username
    const user = await AppDataSource.getRepository(User).findOneBy({
      username: username,
    })
    res.json(user)
  },

  // [GET] /user/info
  getSelfInfo: async function (req: Request, res: Response) {
    const username = req.session.username
    const user = await AppDataSource.getRepository(User).findOneBy({
      username: username,
    })
    res.send(helper.filterPropUser(user))
  },

  // [POST] /user
  postUser: async function (req: Request, res: Response) {
    const user = new User()
    user.username = String(req.body.username)
    user.email = String(req.body.email)
    user.googleId = String(req.body.googleId)
    user.password = String(req.body.password)
    user.role = String(req.body.role)

    await helper.validate(res, user)
    const userRepository = AppDataSource.getRepository(User)
    const result = await userRepository.save(user)
    res.json({ msg: 'Insert complete', result: JSON.stringify(result) })
  },

  // [PUT] /user/:username
  updateUser: async function (req: Request, res: Response) {
    const userRepository = AppDataSource.getRepository(User)
    const username = String(req.params.username)

    try {
      const user = await userRepository.findOneBy({ username: username })
      user.username = String(req.body.username)
      user.email = String(req.body.email)
      user.googleId = String(req.body.googleId)
      user.password = String(req.body.password)
      user.role = String(req.body.role)

      await helper.validate(res, user)
      const result = await userRepository.save(user)

      // const oldUser = await userRepository.findOneBy({ username: username })
      // await userRepository.remove(oldUser)

      res.json({ msg: 'Update complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid username' })
    }
  },

  // [DELETE] /user/:username
  deleteUser: async function (req: Request, res: Response) {
    const userRepository = AppDataSource.getRepository(User)
    const username = String(req.params.username)

    try {
      const video = await userRepository.findOneBy({ username: username })
      const result = await userRepository.remove(video)
      res.json({ msg: 'Delete complete', result: JSON.stringify(result) })
    } catch (error) {
      res.json({ msg: 'Invalid id' })
    }
  },

  // [GET] user/columns
  getColumns: function (req: Request, res: Response) {
    const columns = {
      username: 'string',
      email: 'string',
      googleId: 'string',
      password: 'string',
      role: 'string',
    }
    res.json(columns)
  },
}
