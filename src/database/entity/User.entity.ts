import { Column, Entity, PrimaryColumn } from 'typeorm'
import { IsInt, IsString, Min, IsBoolean } from 'class-validator'

@Entity()
export class User {
  @PrimaryColumn({ type: 'varchar', length: 255 })
  username: string

  @Column({ type: 'varchar', length: 125, nullable: true })
  @IsString()
  email: string

  @Column({ type: 'text', nullable: true })
  @IsString()
  googleId: string

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  password: string

  @Column({ type: 'varchar', length: 125, nullable: true })
  @IsString()
  role: string
}
