import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
  IsNull,
} from 'typeorm'

import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNumber,
  IsString,
  IsUrl,
  Min,
} from 'class-validator'
import { Category } from '../Category.entity'

@Entity()
export class Article {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  active: boolean

  @ManyToOne((type) => Category, (category) => category.articles)
  category: Category

  @Column({ type: 'varchar', length: 255, nullable: true })
  @IsString()
  editor: string

  @Column({ type: 'text' })
  @IsString()
  headline: string

  @Column({ type: 'text' })
  @IsString()
  html: string

  @Column({ type: 'datetime', nullable: true })
  publishedAt: Date

  @Column({ type: 'text', nullable: true })
  @IsUrl()
  thumbnail: string

  @Column({ type: 'int', unsigned: true, default: 0 })
  views: number

  @BeforeInsert()
  updatePublishedBeforeInsert() {
    this.publishedAt = new Date()
  }

  @AfterUpdate()
  updatePublishedBeforeUpdate() {
    if (!this.publishedAt) this.publishedAt = new Date()
  }
}
