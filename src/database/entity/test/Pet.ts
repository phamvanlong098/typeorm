import { get } from 'http'
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm'
import { Human } from './Human'
@Entity()
export class Pet {
  @PrimaryGeneratedColumn()
  id: number

  @Column('text')
  name: string

  @ManyToOne((type) => Human, (human) => human.pets)
  boss: Human
}
