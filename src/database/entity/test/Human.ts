import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { Pet } from './Pet'

@Entity()
export class Human {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  lastName: string

  @OneToMany((type) => Pet, (pet) => pet.boss)
  pets: Pet[]
}
