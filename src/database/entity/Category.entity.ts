import { IsInt, IsString, Min, IsBoolean } from 'class-validator'
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Article } from './news/Article.entity'

@Entity()
export class Category {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  active: boolean

  @Column({ type: 'varchar', length: 255 })
  @IsString()
  name: string

  @OneToMany((type) => Article, (article) => article.category)
  articles: Article[]
}
