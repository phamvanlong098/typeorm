import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  BeforeInsert,
  AfterUpdate,
  ManyToOne,
} from 'typeorm'
import { IsBoolean, IsDate, IsUrl, IsInt, IsString, Min } from 'class-validator'
import { Category } from '../Category.entity'

@Entity()
export class Video {
  @PrimaryGeneratedColumn({ type: 'int', unsigned: true })
  id: number

  @Column({ type: 'boolean', default: false })
  @IsBoolean()
  active: boolean

  @ManyToOne((type) => Category, (category) => category.articles)
  category: Category

  @Column({ type: 'varchar', length: 255, nullable: true })
  @IsString()
  editor: string

  @Column({ type: 'text', nullable: true })
  @IsUrl()
  thumbnail: string

  @Column({ type: 'text' })
  @IsString()
  title: string

  @Column()
  @IsDate()
  publishedAt: Date

  @Column({ type: 'text' })
  @IsUrl()
  videoUrl: string

  @BeforeInsert()
  updatePublishedBeforeInsert() {
    this.publishedAt = new Date()
  }

  @AfterUpdate()
  updatePublishedBeforeUpdate() {
    if (!this.publishedAt) this.publishedAt = new Date()
  }
}
