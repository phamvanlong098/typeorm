import { AppDataSource } from './data-source'
import { Brand } from './entity/test/Brand'
import { Human } from './entity/test/Human'
import { Pet } from './entity/test/Pet'

export default AppDataSource.initialize()
  .then(async () => {
    const result = await AppDataSource.createQueryBuilder()
      .select('human')
      .from(Human, 'human')
      .innerJoinAndSelect('human.pets', 'pets')
      .where('human.id = 2')
      .getMany()

    console.log(JSON.stringify(result))
  })
  .catch((err) => {
    console.error('Error during Data Source initialization', err)
  })
