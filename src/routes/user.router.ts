import { Router } from 'express'
import { Request, Response } from 'express'
import userController from '../controllers/user.controller'

const router = Router()

router.get('/', userController.getUsers)
router.get('/info', userController.getSelfInfo)
router.get('/columns', userController.getColumns)
router.get('/:username', userController.getUser)
router.post('/', userController.postUser)
router.put('/:username', userController.updateUser)
router.delete('/:username', userController.deleteUser)

export default router
