import { Router } from 'express'
import { Request, Response } from 'express'
import articleController from '../controllers/article.controller'
const router = Router()

router.get('/', articleController.getArticles)
router.get('/columns', articleController.getColumns)
router.get('/:id', articleController.getArticle)
router.post('/', articleController.postArticle)
router.put('/:id', articleController.updateArticle)
router.delete('/:id', articleController.deleteArticle)

export default router
