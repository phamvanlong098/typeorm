import { Router } from 'express'
import { Request, Response } from 'express'
import videoController from '../controllers/video.controller'

const router = Router()

router.get('/', videoController.getVideos)
router.get('/columns', videoController.getColumns)
router.get('/:id', videoController.getVideo)
router.post('/', videoController.postVideo)
router.put('/:id', videoController.updateVideo)
router.delete('/:id', videoController.deleteVideo)

export default router
