import { Router } from 'express'
import { Request, Response } from 'express'
import categoryController from '../controllers/category.controller'
const router = Router()

router.get('/', categoryController.getCategories)
router.get('/columns', categoryController.getColumns)
router.get('/:id', categoryController.getCategory)
router.post('/', categoryController.postCategory)
router.put('/:id', categoryController.updateCategory)
router.delete('/:id', categoryController.deleteCategory)

router.get('/:categoryId/article', categoryController.getArticles)
router.get('/:categoryId/video', categoryController.getVideos)

export default router
