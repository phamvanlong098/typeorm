import * as path from 'path'
import * as Express from 'express'
import { Request, Response } from 'express'
import userRouter from './user.router'
import videoRouter from './video.router'
import articleRouter from './article.router'
import categoryRouter from './category.router'
import apiRouter from './api.v1.router'
import authController from '../controllers/auth.controller'
import auth from '../middlewares/auth'
import upload from '../middlewares/multer'
import userController from '../controllers/user.controller'

const maxSize = 10 * 1024 *1024 * 8 ; // 2MB
export default function routes(app) {
  // dont have to login
  app.get('/', (req: Request, res: Response) => {
    res.sendFile(path.join(__dirname, 'index.html'))
  })

  app.post('/createDefault', userController.postUser)

  app.post('/login', authController.login)
  app.use('/api/v1', apiRouter)

  // need login
  app.use('/', auth.checkToken)

  // --- Long--------------
  //file upload - ckeditor upload
  app.post('/uploadfile', upload.single('upload'), (req, res) => {
    const file = req.file
    if (!file) {
      res.join({
        msg: 'Please upload a file',
      })
    } else {
      if (file.size > maxSize) {
                    return res.status(500).send({
                        message: "File size cannot be larger than 2MB!",
                    });}
      else {          
        res.json({
          url: process.env.BASE_URL + `/uploads/${file.filename}`,
          default: process.env.BASE_URL + `/uploads/${file.filename}`,
        })
    }
  }
  })
 // ---- end Long-----------

  app.post('/logout', authController.logout)

  app.use('/user', userRouter)
  app.use('/article', articleRouter)
  app.use('/category', categoryRouter)
  app.use('/video', videoRouter)

  // catch
  app.use('/', (req: Request, res: Response) => {
    let response = { msg: '404 Not found' }
    res.status(404).json(Object.assign({}, response))
  })
}
