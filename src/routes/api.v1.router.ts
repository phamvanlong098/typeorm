import { Router } from 'express'
import { Request, Response } from 'express'
import categoryController from '../controllers/category.controller'
import videoController from '../controllers/video.controller'
import articleController from '../controllers/article.controller'

const router = Router()

router.get('/video', videoController.getVideos)
router.get('/video/:id', videoController.getVideo)
router.get('/article', articleController.getArticles)
router.get('/article/:id', articleController.getArticle)
router.get('/category', categoryController.getCategories)
router.get('/category/:categoryId/article', categoryController.getArticles)
router.get('/category/:categoryId/video', categoryController.getVideos)

export default router
