import { Request, Response } from 'express'
import * as jwt from 'jsonwebtoken'

export const helper = {
  getToken(data = {}, secret) {
    const token = jwt.sign({ data }, secret, { expiresIn: '30m' })
    return token
  },
  setToken(req: Request, data = {}) {
    const token = this.getToken(data, req.session.secret)
    req.session.token = token
    return token
  },
}

export default {
  checkToken(req: Request, res: Response, next) {
    if (!req.session.token) {
      res.status(401).json({ msg: 'Please login first' })
    } else {
      try {
        var decoded = jwt.verify(req.session.token, req.session.secret)
        next()
      } catch (err) {
        res.status(401).json({ msg: 'Please login first' })
      }
    }
  },
}
