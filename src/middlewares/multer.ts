const multer = require('multer')
const multerStorage = multer.memoryStorage();
const maxSize = 10 * 1024 *1024 * 8 ; // 2MB


// check typefile
const multerFilter = (req, file, cb) => {
    if (file.mimetype.startsWith("image") || file.mimetype.startsWith("video")) {
      cb(null, true);
    } else {
      cb("Please upload only images or videos.", false);
    }
  };

  // validate:  size of img
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  },
  storage: multerStorage,
  limits: { fileSize: maxSize }
})

var upload = multer({ storage: storage,  fileFilter: multerFilter })

export default upload


// const util = require("util");
// const multer = require("multer");
// const maxSize = 2 * 1024 * 1024;

// let storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//       cb(null, 'public/uploads')
//     },
//     filename: (req, file, cb) => {
//         console.log(file.originalname);
//         cb(null, file.originalname);
//     },
// });

// let uploadFile = multer({
//     storage: storage,
//     limits: { fileSize: maxSize },
// }).single("file");

// let uploadFileMiddleware = util.promisify(uploadFile);
// export def uploadFileMiddleware;
